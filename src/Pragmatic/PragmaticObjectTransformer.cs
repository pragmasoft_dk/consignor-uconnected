﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UConnector.Config;
using UConnector.Framework;
using UConnector.Samples.Pragmatic.Datatypes.Base;

namespace UConnector.Samples.Pragmatic
{
    public class PragmaticObjectTransformer : ITransformer<PragmaticTransformable, PragmaticTransformable>
    {
        public PragmaticTransformable Execute(PragmaticTransformable input)
        {
            return SetValuesInOutput(input);
        }

        private PragmaticTransformable SetValuesInOutput(PragmaticTransformable input)
        {
            input.OutputFileType = GetTemplateFileType(input);

            var tempTemplate = GetTemplate(input);
            //var rgx = new Regex("[^a-zA-Z0-9 -]");

            /**
             * Searches the template file for matches with each KeyValuePair, and replaces key with value(without hardbrackets) if found.
             * If not in debugmode (.config), keys with no value is replaced with "".
             */
            foreach (var valuePair in input.KeyValuePairs)
            {
                if (valuePair != string.Empty && valuePair.Contains(","))
                {
                    var pair = valuePair.Split(',');

                    var str = pair[1].Replace("]", "");
                    str = str.Replace("[", "");

                    if (int.Parse(GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectTransformer", "IsInDebugMode")) != 1)
                    {
                        tempTemplate = tempTemplate.Replace(pair[0], str);
                    }

                    if (int.Parse(GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectTransformer", "IsInDebugMode")) == 1)
                    {
                        if (str == string.Empty)
                        {
                            tempTemplate = tempTemplate.Replace(pair[0], pair[0]);    
                        } else if (str != string.Empty)
                        {
                            tempTemplate = tempTemplate.Replace(pair[0], str);
                        }
                    }
                }
            }

            input.OutputToFile = tempTemplate;

            return input;
        }

        private string GetTemplateFileType(PragmaticTransformable input)
        {
            var fileType = "";

            string[] filePaths = Directory.GetFiles(GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectTransformer", "TemplateFileDir"));

            foreach (var filePath in filePaths)
            {
                if (filePath.Contains(input.TemplateDefinition))
                {
                    fileType = "." + filePath.Split('.').Last();
                }
            }

            return fileType;
        }

        private string GetTemplate(PragmaticTransformable input)
        {
            string[] filePaths = Directory.GetFiles(GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectTransformer", "TemplateFileDir"));

            var template = "";

            foreach (var filePath in filePaths)
            {
                if (filePath.Contains(input.TemplateDefinition))
                {
                    var filestream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

                    using (var reader = new StreamReader(filestream))
                    {
                        string read;

                        while ((read = reader.ReadLine()) != null)
                        {
                            template += (read + "\n");
                        }
                    }
                }
            }

            return template;
        }

        private string GetConfigurationValue(string operationName, string configurationName, string optionName)
        {
            var path = new OperationConfigurationReader();

            foreach (var operationSectionItem in path.GetOperationConfigurations())
            {
                if (operationSectionItem.Section.Name.Contains(operationName))
                {
                    var temp = operationSectionItem;
                    var anotherTemp = temp.Section.Configs.AddOrGet(configurationName);
                    return anotherTemp.Options.AddOrGet(optionName, true).Value;
                }
            }
            return "";
        }
    }
}
