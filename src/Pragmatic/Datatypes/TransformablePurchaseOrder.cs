﻿using System.Collections.Generic;
using UConnector.Samples.Pragmatic.Datatypes.Base;

namespace UConnector.Samples.Pragmatic.Datatypes
{
    public class TransformablePurchaseOrder : PragmaticTransformable
    {
        //Contains property name and value. Syntax is: [Name][Value]
        public override IEnumerable<string> KeyValuePairs { get; set; }

        //Defines the name of desired template file.
        public override string TemplateDefinition { get; set; }

        //Defines the output folder name.
        public override string OutputFolderName { get; set; }

        //Holds the string to be written to output file. Is set in the transformer.
        public override string OutputToFile { get; set; }

        //Defines the output file type. Is the same as the template file type.
        public override string OutputFileType { get; set; }

        //Defines the output file name.
        public override string OutputFileName { get; set; }

        //Identifier to be used in IPragmaticObjectEditor.
        public override int ObjectIdentifier { get; set; }
    }
}
