﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Linq;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Transactions;
using UConnector.Framework;

namespace UConnector.Samples.Helpers
{
    public class PurchaseOrderStatusEditor
    {
        public void UpdateOrderStatus(IEnumerable<int> orderId)
        {
            // This guy handles the status change including running any configured pipeline
            var orderService = ObjectFactory.Instance.Resolve<IOrderService>();

            var existingOrderStatus = new OrderStatus(3);//OrderStatus.Get((int)OrderStatusCode.CompletedOrder);
            var newOrderStatus = new OrderStatus(2);//OrderStatus.Get((int)OrderStatusCode.NewOrder);

            var purchaseOrders = orderId.Select(i => PurchaseOrder.Find(x => x.OrderId == i).First()).ToList();

            foreach (var order in purchaseOrders)
            {
                orderService.ChangeOrderStatus(order, newOrderStatus);
                order.Save();
            }
        }
    }
}
